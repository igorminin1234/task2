package org.ihor.minin;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.Scanner;

public class FileView {
    public void showFile(String fileName) {
        try {
            Scanner scan = new Scanner(new FileReader(fileName));
            while (scan.hasNextLine()) {
                System.out.println(scan.nextLine() + System.lineSeparator());
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

    }
}

