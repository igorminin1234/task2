package org.ihor.minin;

public class Main {
    public static void main(String[] args) {
        showFileView("text.txt");
    }

    public static void showFileView(String fileName) {
        new FileView().showFile(fileName);
    }
}
